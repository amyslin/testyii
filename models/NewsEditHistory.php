<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%news_edit_history}}".
 *
 * @property integer $id
 * @property integer $news_item_id
 * @property string $c_time
 * @property string $m_time
 * @property string $action
 *
 * @property NewsItem $newsItem
 */
class NewsEditHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_edit_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_item_id'], 'integer'],
            [['c_time', 'm_time'], 'safe'],
            [['action'], 'string'],
            [['news_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewsItem::className(), 'targetAttribute' => ['news_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_item_id' => 'News Item ID',
            'c_time' => 'C Time',
            'm_time' => 'M Time',
            'action' => 'Action',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsItem()
    {
        return $this->hasOne(NewsItem::className(), ['id' => 'news_item_id']);
    }
}
