<?php

namespace app\models;

use Yii;
use yii\base\Event;

/**
 * This is the model class for table "{{%news_item}}".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $c_time
 * @property string $m_time
 * @property string $publish_time
 * @property integer $activity
 * @property string $title
 * @property string $preview_text
 * @property string $preview_img
 * @property string $detail_text
 * @property string $detail_img
 *
 * @property NewsEditHistory[] $newsEditHistories
 * @property User $owner
 */
class NewsItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'activity'], 'integer'],
            [['c_time', 'm_time', 'publish_time'], 'safe'],
            [['preview_text', 'detail_text'], 'string'],
            [['title', 'preview_img', 'detail_img'], 'string', 'max' => 256],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => '�� ������������',
            'c_time' => '����� ��������',
            'm_time' => '����� ���������',
            'publish_time' => '����� ����������',
            'activity' => '����������',
            'title' => '���������',
            'preview_text' => '����� ������',
            'preview_img' => '�������� ������',
            'detail_text' => '��������� �����',
            'detail_img' => '��������� ��������',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsEditHistories()
    {
        return $this->hasMany(NewsEditHistory::className(), ['news_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
    
    protected function afterSave($insert, $changedAttributes) {
    	if (!empty($changedAttributes['activity']))
    		yii::$app->trigger('app.news.publish', new yii\base\Event(['sender'=>$this]));
    	return parent::afterSave($insert, $changedAttributes);
    }
}
