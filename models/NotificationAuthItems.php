<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%notification_auth_items}}".
 *
 * @property integer $notification_id
 * @property integer $auth_item_name
 * @property string $c_time
 * @property string $m_time
 *
 * @property Notification $notification
 */
class NotificationAuthItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_auth_items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'auth_item_name'], 'required'],
            [['notification_id', 'auth_item_name'], 'integer'],
            [['c_time', 'm_time'], 'safe'],
            [['notification_id'], 'exist', 'skipOnError' => true, 'targetClass' => Notification::className(), 'targetAttribute' => ['notification_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_id' => 'Notification ID',
            'auth_item_name' => 'Auth Item Name',
            'c_time' => 'C Time',
            'm_time' => 'M Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['id' => 'notification_id']);
    }
}
