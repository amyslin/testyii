<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%notification}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $events
 * @property string $template
 * @property string $c_time
 * @property string $m_time
 * @property integer $owner_id
 * @property string $title
 *
 * @property NotificationType $type0
 * @property User $owner
 * @property NotificationAuthItems[] $notificationAuthItems
 * @property NotificationItem[] $notificationItems
 * @property NotificationUsers[] $notificationUsers
 * @property User[] $users
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template'], 'string'],
            [['c_time', 'm_time'], 'safe'],
            [['owner_id'], 'integer'],
            [['name', 'events', 'title'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50],
            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => NotificationType::className(), 'targetAttribute' => ['type' => 'name']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'events' => 'Events',
            'template' => 'Template',
            'c_time' => 'C Time',
            'm_time' => 'M Time',
            'owner_id' => 'Owner ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(NotificationType::className(), ['name' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationAuthItems()
    {
        return $this->hasMany(NotificationAuthItems::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationItems()
    {
        return $this->hasMany(NotificationItem::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationUsers()
    {
        return $this->hasMany(NotificationUsers::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%notification_users}}', ['notification_id' => 'id']);
    }
}
