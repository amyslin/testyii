<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%notification_item}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $notification_id
 * @property string $c_time
 * @property string $m_time
 * @property string $title
 * @property string $text
 * @property string $status
 *
 * @property Notification $notification
 * @property User $user
 */
class NotificationItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'notification_id'], 'integer'],
            [['c_time', 'm_time'], 'safe'],
            [['text', 'status'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['notification_id'], 'exist', 'skipOnError' => true, 'targetClass' => Notification::className(), 'targetAttribute' => ['notification_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'notification_id' => 'Notification ID',
            'c_time' => 'C Time',
            'm_time' => 'M Time',
            'title' => 'Title',
            'text' => 'Text',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['id' => 'notification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
