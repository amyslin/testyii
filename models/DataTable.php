<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\db\ActiveQuery;


/**
 * This is the model class for table "data".
 *
 * @property integer $id
 * @property string $card_number
 * @property string $date
 * @property double $volume
 * @property string $service
 * @property integer $address_id
 */
class DataTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'volume', 'service'], 'required'],
            [['date'], 'safe'],
            [['volume'], 'number'],
            [['address_id'], 'integer'],
            [['card_number'], 'string', 'max' => 20],
            [['service'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_number' => 'Card Number',
            'date' => 'Date',
            'volume' => 'Volume',
            'service' => 'Service',
            'address_id' => 'Address ID',
        	'year' => 'Year',
        	'month' => 'Month'
        ];
    }
    
    public function getYear() {
    	return Yii::$app->formatter->asDate($this->date, 'yyyy');
    }
    
    public function  getMonth() {
    	return Yii::$app->formatter->asDate($this->date, 'MM');
    }
    
    public static function getCountByYears() {
    	return self::find()
    		->select(["DATE_FORMAT(date, '%Y') as year", "COUNT(*) AS count"])
    		->groupBy(new Expression("DATE_FORMAT(date, '%Y')"))
    		->orderBy (new Expression("DATE_FORMAT(date, '%Y') DESC"))
    		->asArray(true)
    		->all();
    }
    
    public static function getCountByMonth($year = null) {
    	$data = self::find()
    		->select(["DATE_FORMAT(date, '%Y') as year", "DATE_FORMAT(date, '%m') as month", "COUNT(*) AS count"])
    		->groupBy(new Expression("DATE_FORMAT(date, '%Y-%m')"))
    		->orderBy (new Expression("DATE_FORMAT(date, '%m') DESC"))
//     		->where("DATE_FORMAT(date, '%Y') = :year", [":year"=>empty($year)?date('Y'):$year])
    		->asArray(true)
    		->all();
    	$result = [];
    	foreach ($data as $item) {
    		$result[$item['year']]['items'][] = [
    			'name'=>$item['month'],
    			'count'=>$item['count']
    		];
    		$result[$item['year']]['count'] = (empty($result[$item['year']]['count']))?$item['count']:$result[$item['year']]['count']+$item['count'];
    	}
    	return $result;
    }
    
}
