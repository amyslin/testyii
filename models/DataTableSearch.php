<?php

namespace app\models;

use Yii;
use app\models\DataTable;
use yii\data\ActiveDataProvider;

class DataTableSearch extends DataTable {
	
	public $card_number;
	public $year;	
	public $month;
	
	public function rules() {
		return [
				['year', 'match', 'pattern'=>'/^\d{4}$/'],
				['month', 'match', 'pattern'=>'/^\d{2}$/'],
				['card_number', 'string', 'max' => 20],
		];
	}
	
	public function search($params) {
		$query = DataTable::find();
		$dataProvider = new ActiveDataProvider([
				'query'=>$query,
// 				'pagination'=> [
// 						'pageSize'=>25,
// 				],
				'sort'=>[
						'defaultOrder' => [
								'date' => SORT_DESC,
						]
				]
		]);
		
		$this->load($params);
		if($year = Yii::$app->request->get('year'))
			$this->year = $year;
		if($month = Yii::$app->request->get('month'))
			$this->month = $month;
		
		if (!$this->validate()) 
			return $dataProvider;

		$query->andFilterWhere([
				"DATE_FORMAT(date, '%Y')"=>$this->year,
				"DATE_FORMAT(date, '%m')"=>$this->month,
		]);
		$query->andFilterWhere(['like', 'card_number', $this->card_number]);
		
		return $dataProvider;
	}
	
	
}