<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%notification_type}}".
 *
 * @property string $name
 * @property string $description
 * @property string $c_time
 * @property string $m_time
 * @property integer $owner_id
 *
 * @property Notification[] $notifications
 * @property User $owner
 */
class NotificationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['c_time', 'm_time'], 'safe'],
            [['owner_id'], 'integer'],
            [['name', 'description'], 'string', 'max' => 50],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'description' => 'Description',
            'c_time' => 'C Time',
            'm_time' => 'M Time',
            'owner_id' => 'Owner ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['type' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
