<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


class UploadForm extends Model {
	public $filename;
	
	public function rules() 
	{
		return [
				[['filename'], 'file', 'skipOnEmpty'=>false, 'extensions'=>'zip']
		];
	}
	
	
}