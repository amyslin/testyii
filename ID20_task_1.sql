﻿DROP TEMPORARY TABLE IF EXISTS data_1;
CREATE TEMPORARY TABLE data_1 ENGINE MEMORY
AS (SELECT 
  d.id,
  d.card_number,
  d.date,
  SUM(d.volume) AS volume,
  d.service,
  d.address_id,
  d1.id AS tmp_id,
  d1.date AS tmp_date,
  d1.volume AS tmp_volume
  FROM data d  
  LEFT JOIN 
  data d1 USING(card_number, address_id)
  WHERE d.volume > 0
  AND d1.id = (
    SELECT id FROM data 
    WHERE card_number = d.card_number 
      AND address_id = d.address_id 
      AND volume < 0 
      AND d.date > date 
    ORDER BY date DESC 
    LIMIT 1
  )
  GROUP BY d.id
  ORDER BY d.date ASC);
#=== Проверка что не осталось минусовых значений ===
#SELECT * FROM data d 
#  WHERE d.id NOT IN 
#(SELECT id FROM data_1 d1) AND d.volume > 0;

#=== Суммируем значения ===
UPDATE data d 
  INNER JOIN data_1 d1 ON d.id = d1.tmp_id 
  set d.volume = d.volume + d1.volume;
#=== Удаляем возвраты ===
DELETE FROM data WHERE id IN (SELECT id FROM data_1);
DROP TEMPORARY TABLE IF EXISTS data_1;
/*
В дампе имеются транзакции с 0 значением, по этой причине при выполнении запроса остается 1 запись
*/
SELECT 
  d.id,
  d.card_number,
  d.date,
  SUM(d.volume) AS volume,
  d.service,
  d.address_id,
  d1.id AS tmp_id,
  d1.date AS tmp_date,
  d1.volume AS tmp_volume
  FROM data d  
  LEFT JOIN 
  data d1 USING(card_number, address_id)
  WHERE d.volume > 0
  AND d1.id = (SELECT id FROM data WHERE card_number = d.card_number AND address_id = d.address_id AND volume < 0 AND d.date > date ORDER BY date DESC LIMIT 1)
  GROUP BY d.id
  ORDER BY d.date ASC;
