<?php
use yii\widgets\ActiveForm
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
	<?= $form->field($model, 'filename')->fileInput() ?>
	<button type="submit">Обработать</button>
	
<?php ActiveForm::end();?>