<?php 

// use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\base\Widget;
use yii\widgets\Menu;
use yii\helpers\Url;
use yii\grid\GridView;
// use yii\grid\DataColumn;

$this->title = 'Тестовое задание ID20';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
	<div class="col-md-3">
	<?php 
	$items = [];
	foreach ($menu as $key => $item) {
		$it = [];
		$it['label'] = '<span class="badge pull-right">' .$item['count'].'</span><b>'.$key.'</b>';
		$it['encode'] = false;
		$it['options'] = ['class'=>'dropdown'];
// 		$it['template'] = '<a href="{url}" ><b>{label}</b></a>';
		$it['url'] = Url::to(['id/index','year'=>$key]);
		$it['active'] = ($year==$key);
		$it['items'] = [];
		foreach ($item['items'] as $subItem) {
			$it['items'][] = [
					'label' =>'<span class="badge pull-right">'.$subItem['count'].'</span>&nbsp;&nbsp;'.Yii::$app->formatter->asDate($key . '-' .$subItem['name'].'-01', 'LLLL'),
					'encode'=>false,
					'active'=>($year==$key&&$subItem['name']==$month),
					'url' => Url::to(['id/index', 'year'=>$key, 'month'=>$subItem['name']])
			];
		}
		$items[] = $it;
	}
	echo Menu::widget([
			'items'=>$items,
			'options'=> [
				'class'=>'nav nav-pills nav-stacked',
			],
			'activeCssClass'=>'active',
			'submenuTemplate'=>'<ul class="nav nav-pills nav-stacked">{items}</ul>',
		]);
	?>
	</div>
	<div class="col-md-9">
	 <?php 
	 echo GridView::widget([
	 		'dataProvider'=>$provider,
	 		'filterModel' => $searchModel,
// 	 		'formatter'=>[[
// 	 			'class'=>DataColumn::className(),
// 	 			'attribute'=>'data',
// 	 			'format'=>'datetime'
// 	 		]]
	 ]);
	 
	 ?>
	</div>
</div>
