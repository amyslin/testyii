<?php

use yii\db\Migration;

class m170725_141509_init_notification extends Migration
{
    public function safeUp()
    {
    	$this->createTable('{{%notification_type}}', [
    		'name' => $this->string(50)->notNull(),
    		'description' => $this->string(50),
    		'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
    		'm_time' => $this->dateTime(),
    		'owner_id' => $this->integer(),
    		'PRIMARY KEY ([[name]])',
    	]);
    	
    	$this->addForeignKey(
    		'FK_notification_type_yii_user_id',
    		'{{%notification_type}}',
    		'owner_id',
    		'{{%user}}',
    		'id',
    		'CASCADE'
    	);
    	
    	$this->createTable('{{%notification}}', [
    		'id' => $this->primaryKey(),
    		'name' => $this->string(),
    		'type' => $this->string(50),
    		'events' => $this->string(255),
    		'template' => $this->text(),
    		'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
    		'm_time' => $this->dateTime(),
    		'owner_id' => $this->integer(),
    		'title' => $this->string(255),
    	]);
    	
    	$this->addForeignKey(
    		'FK_yii_notification_yii_notification_type_name',
    		'{{%notification}}',
    		'type',
    		'{{%notification_type}}',
    		'name',
    		'SET NULL'
    	);
    	
    	$this->addForeignKey(
    		'FK_yii_notification_yii_user_id',
    		'{{%notification}}',
    		'owner_id',
    		'{{%user}}',
    		'id',
    		'CASCADE'
    	);
    	
    	
    	$this->createTable('{{%notification_item}}', [
    		'id' => $this->primaryKey(),
    		'user_id' => $this->integer(),
    		'notification_id' => $this->integer(),
    		'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
    		'm_time' => $this->dateTime(),
    		'title' => $this->string(255),
    		'text' => $this->text(),
    		'status' => 'ENUM("new", "panding", "sent", "error")',
    	]);
    	
    	$this->addForeignKey(
    		'FK_yii_notification_item_yii_user_id',
    		'{{%notification_item}}',
    		'user_id',
    		'{{%user}}',
    		'id',
    		'SET NULL'
    	);
    
    	$this->addForeignKey(
    		'FK_yii_notification_item_yii_notification_id',
    		'{{%notification_item}}',
    		'notification_id',
    		'{{%notification}}',
    		'id',
    		'SET NULL'
    	);
    	
    	
    	$this->createTable('{{%notification_users}}', [
    		'notification_id' => $this->integer(),
    		'user_id' => $this->integer(),
    		'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
    		'm_time' => $this->dateTime(),
    		'PRIMARY KEY ([[notification_id]], [[user_id]])',
    	]);
    	
    	$this->addForeignKey(
    		'FK_yii_notification_users_yii_notification_id',
    		'{{%notification_users}}',
    		'notification_id',
    		'{{%notification}}',
    		'id',
    		'CASCADE'
    	);
    	
    	$this->addForeignKey(
    		'FK_yii_notification_users_yii_user_id',
    		'{{%notification_users}}',
    		'user_id',
    		'{{%user}}',
    		'id',
    		'CASCADE'
    	);
    	
    	$this->createTable('{{%notification_auth_items}}', [
    		'notification_id' => $this->integer(),
    		'auth_item_name' => $this->integer(),
    		'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
    		'm_time' => $this->dateTime(),
    		'PRIMARY KEY ([[notification_id]], [[auth_item_name]])',
    	]);
    	
    	$this->addForeignKey(
    		'FK_yii_notification_auth_items_yii_notification_id',
    		'{{%notification_auth_items}}',
    		'notification_id',
    		'{{%notification}}',
    		'id',
    		'CASCADE'
    	);
    	
    	
    }

    public function safeDown()
    {
    	$this->dropForeignKey(
    		'FK_notification_type_yii_user_id',
    		'{{%notification_type}}'
    	);
    	
    	$this->dropForeignKey(
    		'FK_yii_notification_yii_notification_type_name',
    		'{{%notification}}'
    	);
    	
    	$this->dropForeignKey(
    		'FK_yii_notification_yii_user_id',
    		'{{%notification}}'
    	);
    	
    	$this->dropForeignKey(
    		'FK_yii_notification_item_yii_user_id',
    		'{{%notification_item}}'
    	);
    	
    	$this->dropForeignKey(
    		'FK_yii_notification_item_yii_notification_id',
    		'{{%notification_item}}'
    	);
    	
    	$this->dropForeignKey(
    		'FK_yii_notification_users_yii_user_id',
    		'{{%notification_users}}'
    	);
    	
    	$this->dropForeignKey(
    		'FK_yii_notification_users_yii_notification_id',
    		'{{%notification_users}}'
    	);
    	
    	$this->dropForeignKey(
    		'FK_yii_notification_auth_items_yii_notification_id',
    		'{{%notification_auth_items}}'
    	);
    	
    	$this->dropTable('{{%notification}}');    	
    	$this->dropTable('{{%notification_type}}');
    	$this->dropTable('{{%notification_item}}');
    	$this->dropTable('{{%notification_users}}');
    	$this->dropTable('{{%notification_auth_items}}');
		
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170725_141509_init_notification cannot be reverted.\n";

        return false;
    }
    */
}
