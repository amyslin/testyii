<?php

use yii\db\Migration;

/**
 * Handles the creation of table `yii_user`.
 */
class m170725_054951_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
        	'username' => $this->string()->notNull(),
        	'password' => $this->string(),
        	'authKey' => $this->string(),
        	'accessToken' => $this->string(),
        	'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
        	'm_time' => $this->dateTime(),
        	'activity' => $this->boolean()->notNull()->defaultValue(false),
        	'last_login_time' =>$this->dateTime(),
        ]);
      
        $this->insert('{{%user}}', [
        	'username' => 'admin',
        	'password' => yii::$app->getSecurity()->generatePasswordHash('admin'),
        	'authKey' => yii::$app->getSecurity()->generateRandomKey(32),
        	'accessToken' => yii::$app->getSecurity()->generateRandomKey(32),
        	'activity' => true,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
