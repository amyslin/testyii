<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_edit_history`.
 */
class m170725_110705_create_news_edit_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%news_edit_history}}', [
            'id' => $this->primaryKey(),
        	'news_item_id' => $this->integer(),
        	'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
        	'm_time' => $this->dateTime(),
        	'action' => 'ENUM("update", "create", "activity")',
        ]);
        
        $this->addForeignKey(
        	'FK_yii_news_edit_history_yii_news_item_id',
        	'{{%news_edit_history}}',
        	'news_item_id',
        	'{{%news_item}}',
        	'id',
        	'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropForeignKey(
    		'FK_yii_news_edit_history_yii_news_item_id',
    		'{{%news_edit_history}}'
    	);
    	
        $this->dropTable('{{%news_edit_history}}');
    }
}
