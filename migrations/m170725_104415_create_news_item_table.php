<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_item`.
 */
class m170725_104415_create_news_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%news_item}}', [
            'id' => $this->primaryKey(),
        	'owner_id' => $this->integer(),
        	'c_time' => $this->dateTime()->notNull()->defaultExpression("NOW()"),
        	'm_time' => $this->dateTime(),
        	'publish_time' => $this->dateTime(),
        	'activity' => $this->boolean()->notNull()->defaultValue(false),
        	'title' => $this->string(256),
        	'preview_text' => $this->text(),
        	'preview_img' => $this->string(256),
        	'detail_text' => $this->text(),
        	'detail_img' => $this->string(256),
        ]);
        
        $this->addForeignKey(
        	'FK_yii_news_item_yii_user_id', 
        	'{{%news_item}}', 
        	'owner_id',
        	'{{%user}}',
        	'id', 
        	'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	
    	$this->dropForeignKey(
    		'FK_yii_news_item_yii_user_id',
    		'{{%news_item}}'
    	);
    	
        $this->dropTable('{{%news_item}}');
    }
}
