<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\rbac\AuthorRule;
use app\models\User;


class RbacController extends Controller {
	public function actionInit() {
		$authManager = Yii::$app->authManager;
		
		$authManager->removeAll();
		$guest = $authManager->createRole('quest');
		$admin = $authManager->createRole('admin');
		$editor = $authManager->createRole('editor');
		$brand  = $authManager->createRole('BRAND');
		$talent = $authManager->createRole('TALENT');
		
		$authManager->add($guest);
		$authManager->add($admin);
		$authManager->add($editor);
		$authManager->add($brand);
		$authManager->add($talent);
		
		$login  = $authManager->createPermission('login');
		$logout = $authManager->createPermission('logout');
		$error  = $authManager->createPermission('error');
		$signUp = $authManager->createPermission('sign-up');
		$index  = $authManager->createPermission('index');
		$preview = $authManager->createPermission('preview');
		$view   = $authManager->createPermission('view');
		$create = $authManager->createPermission('create');
		$update = $authManager->createPermission('update');
		$delete = $authManager->createPermission('delete');
		
		$authManager->add($login);
		$authManager->add($logout);
		$authManager->add($error);
		$authManager->add($signUp);
		$authManager->add($index);
		$authManager->add($view);
		$authManager->add($create);
		$authManager->add($preview);
		$authManager->add($update);
		$authManager->add($delete);
		
		$authorRule = new AuthorRule();
		$authManager->add($authorRule);
		
		
		$editor->ruleName = $authorRule->name;
		$brand->ruleName = $authorRule->name;
		$talent->ruleName = $authorRule->name;
		
		//guest
		$authManager->addChild($guest, $login);
		$authManager->addChild($guest, $logout);
		$authManager->addChild($guest, $error);
		$authManager->addChild($guest, $signUp);
		$authManager->addChild($guest, $index);
		$authManager->addChild($guest, $preview);
		
		//BRAND
		$authManager->addChild($brand, $view);
		$authManager->addChild($brand, $guest);
		
		//TALENT
// 		$authManager->addChild($brand, $view);
		$authManager->addChild($talent, $view);
		$authManager->addChild($talent, $guest);
		
		//editor
		$authManager->addChild($editor, $create);
		$authManager->addChild($editor, $update);
		$authManager->addChild($editor, $delete);
		$authManager->addChild($editor, $brand);
		
		//admin
		
		$authManager->addChild($admin, $editor);
		
		$user = User::findByUsername('admin');
		
		$authManager->assign($admin, $user->id);
		
		
		
		
		
		
		
		
		
		
	}
}