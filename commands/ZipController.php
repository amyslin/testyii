<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\components\ZipProcessor;

class ZipController extends Controller {
	public function actionIndex() {
		//echo Yii::$app->runtimePath;
// 		preg_match('/.*['.preg_quote("/", '/').'|'.preg_quote("\\", '/') .']([^'.preg_quote("/", '/').'|^'.preg_quote("\\", '/') .']+)$/i', '/var/www/html/file.zip', $match);
// 		print_r($match);
		
		ZipProcessor::processTest(Yii::$app->runtimePath . DIRECTORY_SEPARATOR . "Pictures.zip");
		
		//ZipProcessor::process(Yii::$app->runtimePath);
	}
}