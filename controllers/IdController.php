<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\DataTable;
use app\models\DataTableSearch;
// use yii\data\ActiveDataProvider;
// use yii\helpers\ArrayHelper;
// use yii\db\Expression;

class IdController extends Controller {
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
				'error' => [
						'class' => 'yii\web\ErrorAction',
				],
				'captcha' => [
						'class' => 'yii\captcha\CaptchaAction',
						'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
				],
		];
	}
	
	public function actionIndex($year=null, $month = null) {
		
		$menuSource = DataTable::getCountByMonth();		
		$serachModel = new DataTableSearch();
		
		$dataProvider = $serachModel->search(Yii::$app->request->queryParams);
		
		return $this->render('index', ['menu'=>$menuSource, 'year'=>$year, 'month'=>$month, 'provider'=>$dataProvider, 'searchModel'=>$serachModel]);
	}
	
}