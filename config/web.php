<?php
// setlocale(LC_ALL,"ru_RU");
$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
// 	'locale'=>'ru_RU',
	'language' => 'ru_RU',
	'sourceLanguage' => 'ru-RU',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '4SCzNamvczoH75xxXkCJUq-skM2vVhzq',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    	'formatter' => [
    		'class' => 'yii\i18n\Formatter',
    		'nullDisplay' => '&nbsp;',
    		'locale'=>'ru-RU',
    		'thousandSeparator' => ' ',
    		'dateFormat' => 'dd.MM.yyyy',
    		'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
    		'timeFormat' => 'HH:mm:ss',
    	],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    	'authManager' => [
    		'class' => 'yii\rbac\DbManager',
    		'defaultRoles' => ['admin', 'BRAND', 'TALENT']
    	],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        	'enableStrictParsing' => false,
        	//'suffix' => '.html',
            'rules' => [
            		'id/<year:\d{4}>/<month:\d{2}>' => 'id/index',
            		'id/<year:\d{4}>' => 'id/index',
            		'id' => 'id/index',            		
            		'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            		
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
