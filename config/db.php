<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost:3310;dbname=yii2basic',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
	'tablePrefix' => 'yii_',
];
