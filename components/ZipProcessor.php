<?php
/**
 * @author Andrey Myslin <artdevision@gmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * 
 */
namespace app\components;

class ZipProcessor {
	
	private static $_mimeTypes = ['jpg', 'jpeg', 'png', 'bmp', 'gif'];
	private $_renamed = [];
	private $_htmlFiles = [];
	
	/**
	 * Инициализация распаковки и сканирования папки в указанную папку
	 * @param string $path Полный путь к папке для хранения фалов
	 * @return Array Массив обработанных файлов
	 */
	public static function process($path) {
		if(!empty($_FILES)) {
			$outputFiles = [];
			foreach($_FILES as $file) {
				
				$folder = uniqid('zip');
				$fullName = (!empty($file['name']['filename'])) ? $path . ((substr($path, -1, 1) != "/") ? "/" : "") .$folder . "/" . $file['name']['filename'] : null;
				mkdir($path . ((substr($path, -1, 1) != "/") ? "/" : "") .$folder."/");
				if (!empty($file['tmp_name']['filename']) && move_uploaded_file($file['tmp_name']['filename'], $fullName)) {
					if (mb_strtolower(substr($fullName, -3, 3)) != 'zip')
						continue;
					$processor = new ZipProcessor();
					$extractFolder = substr($fullName, 0, -4) . "/";
					
					exec("unzip $fullName -d $extractFolder", $output);
					
					if (!empty($output)) {
						$processor->scanFolder($extractFolder);
						$processor->scanHtml($extractFolder);
					}
					
					$outFileName = $path . ((substr($path, -1, 1) != "/") ? "/" : "") .$folder .".zip";
							
//					exec("zip -rT $outFileName $extractFolder", $out);
					
					self::RZip($extractFolder, $outFileName);
					
					if (file_exists($outFileName))
						$outputFiles[] = $outFileName;
					
				}
				
				
			}
			return $outputFiles;
		}
	}
	
	/**
	 * Инициализация распаковки и сканирования папки в указанную папку
	 * @param string $file Полный путь к файл
	 * @return void
	 */
	public static function processTest($file) {
		$folder = uniqid('zip');
		//$fullName = (!empty($file['name'])) ? $path . ((substr($path, -1, 1) != "/") ? "/" : "") .$folder . $file['name'] : null;
		//if (!empty($file['tmp_name']) && move_uploaded_file($file['tmp_name'], $fullName)) {
			if (mb_strtolower(substr($file, -3, 3)) != 'zip')
				return false;
			//$zip = new \ZipArchive();
			$extractFolder = substr($file, 0, -4) . "/";
			exec("unzip $file -d $extractFolder", $output);
			
			if (!empty($output)) {
				$processor = new ZipProcessor();
				$processor->scanFolder($extractFolder);
				$processor->scanHtml($extractFolder);
				
			}
			
		//}
	}
	
	/**
	 * Сканирование папки на предмет изоражений и транслитерация имен
	 * @param string $folder <br/> Полный путь к сканируемой папке
	 * @return void
	 */
	private function scanFolder($folder) {
		$files = scandir($folder);
		foreach($files as $file) {
			if ($file == '.' || $file == '..')
				continue;
			if (is_dir($folder.$file)) {
				$this->scanFolder($folder.$file."/");	
				continue;
			}
			if(preg_match("/.*(\." . implode("|\.", self::$_mimeTypes) .")$/si", $file)) {
				$newFile = self::translit(iconv('CP866', 'UTF-8', $file));
				$newFilePath = self::nameIndex($folder.$newFile);
				preg_match('/.*['.preg_quote("/", '/').'|'.preg_quote("\\", '/') .']([^'.preg_quote("/", '/').'|^'.preg_quote("\\", '/') .']+)$/i', $newFilePath, $m);
				if(rename($folder.$file, $newFilePath))
					$this->_renamed[iconv('CP866', 'UTF-8', $file)] = (!empty($m[1]))?$m[1]:'';
			}
			
		}
	}
	
	/**
	 * Сканирование папки на HTML файлы, замена изображений
	 * @param string $folder <br/> путь к сканируемой папке
	 * @return void
	 */
	private function scanHtml($folder) {
		$files = scandir($folder);
		foreach($files as $file) {
				if ($file == '.' || $file == '..')
					continue;
				if (is_dir($folder.$file)) {
					$this->scanHtml($folder.$file."/");
					continue;
				}
				if(preg_match("/.*(\." . implode("|\.", ["htm","html"]) .")$/si", $file)) {
					$content = file_get_contents($folder.$file);
					$patterns = [];
					$replacment = [];
					foreach ($this->_renamed as $name => $replace) {
						$patterns[] = 	"/(<img.+src=[\"|'])([^\"|^']*)([".preg_quote("/", "/")."|"
										.preg_quote("\\", "/"). 
										"]{0,1})(". preg_quote($name, "/") .")([\"|'].*[" . preg_quote("/", "/"). ">|>])/mu";
						

						$replacment[] = "$1$2$3" . $replace. "$5";
					}
					$content = preg_replace($patterns, $replacment, $content);
					file_put_contents($folder.$file, $content);
					
				}
				
		}
	}
	
	/**
	 * Индексация имени файла при совпадении
	 * @param string $file <br/> Полный путь до исходного файла
	 * @return string <br/> Проиндексированное имя файла в формате <i><b>name_xx.ext</b></i>
	 */
	private static function nameIndex($file) {
		if (file_exists($file)) {
			if (preg_match("/(.*)(_\d+)(\." . implode("|\.", self::$_mimeTypes) .")$/si", $file, $m)) {
				$index = (isset($m[2]))?intval(substr($m[2], 1))+1:1;
				$l = ($index<100)?2:3;
				return self::nameIndex(preg_replace("/(.*)(_\d+)(\." . implode("|\.", self::$_mimeTypes) .")$/si", "$1_".str_pad($index, $l, "0", STR_PAD_LEFT)."$3", $file));
			}
			else {
				return self::nameIndex(preg_replace("/(.*)(\." . implode("|\.", self::$_mimeTypes) .")$/si", "$1_01$2", $file));
			}
		}
		else
			return $file;
	}
	
	/**
	 * Транслитерация имен файлов
	 * @param string $st <br/> Наименование файла
	 * @return string Имя файла в латинице
	 */
	public static function translit($st, $coder = 'UTF-8') {
		$st = mb_strtolower($st, $coder);
		$russian = [];
		
		$chars = "абвгдежзийклмнопрстуфыэАБВГДЕЖЗИЙКЛМНОПРСТУФЫЭ";
		$charsEn = "abvgdegziyklmnoprstufieABVGDEGZIYKLMNOPRSTUFIE";
	
		for ($i = 0; $i < mb_strlen($chars); $i++) {
			$russian[mb_substr($chars, $i, 1)] = mb_substr($charsEn, $i, 1);
		}
			
		$st = strtr($st, $russian);
		$st = strtr($st, array(
				'ё'=>"yo",    'х'=>"h",  'ц'=>"ts",  'ч'=>"ch", 'ш'=>"sh",
				'щ'=>"shch",  'ъ'=>'',   'ь'=>'',    'ю'=>"yu", 'я'=>"ya",
				'Ё'=>"Yo",    'Х'=>"H",  'Ц'=>"Ts",  'Ч'=>"Ch", 'Ш'=>"Sh",
				'Щ'=>"Shch",  'Ъ'=>'',   'Ь'=>'',    'Ю'=>"Yu", 'Я'=>"Ya",
				' '=>"_"
				
		));
		return preg_replace("/\s+|[[:^print:]]/", "", $st);
	}
	
	/**
	 * Рекурсивное архивирование папки в ZIP
	 * @param string $source <br/> Путь к архивируемой папке
	 * @param string $destination <br/> файл назначения
	 * @return boolean
	 */
	public static function RZip($source, $destination){
		if (!extension_loaded('zip') || !file_exists($source)) {
			return false;
		}
		
		$zip = new \ZipArchive();
		if (!$zip->open($destination, \ZipArchive::CREATE)) {
			return false;
		}
		
		$source = str_replace('\\', '/', realpath($source));
		
		if (is_dir($source) === true){
			$files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);
			
			foreach ($files as $file){
				$file = str_replace('\\', '/', $file);
				
				// Ignore "." and ".." folders
				if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
					continue;
					
					$file = realpath($file);
					$file = str_replace('\\', '/', $file);
					
					if (is_dir($file) === true){
						$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
					}else if (is_file($file) === true){
						$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
					}
			}
		}else if (is_file($source) === true){
			$zip->addFromString(basename($source), file_get_contents($source));
		}
		return $zip->close();
	}
}