Тестовое задание №1 - ID20
================

Задача
----------------
В компании работают водители, которые совершают поездки на корпоративных автомобилях. В течение рабочего времени, они совершают расходы, приобретая услуги (заправка топливом, мойка, шиномонтаж) на различных автозаправках, которые оплачивают топливными картами компании.
В некоторых случаях происходят возвраты средств по приобретенным услугам. Например, водитель попросил оператора заправочной станции заправить автомобиль 50 литрами топлива. При этом оператор совершает списание по топливной карте ДО начала заправки. По окончании заправки выясняется, что в топливный бак вместилось только 48 литров. В этом случае, оператор оформляет возврат средств (2 литра, которые не вместились) на топливную карту.
Данные о приобретенных услугах по топливным картам передаются (в реальном времени) и хранятся в БД.
Таким образом таблица с данным о расходах по топливным картам содержит данные по списанию и возвратам.

### Задание 1

Преобразовать данные таблицы таким образом, чтобы в ней содержались ТОЛЬКО транзакции-расходы. То есть, все транзакции-возвраты должны быть учтены в предшествующих им транзакциях-расходах.

**РЕШЕНИЕ**

```mysql
DROP TEMPORARY TABLE IF EXISTS data_1;
CREATE TEMPORARY TABLE data_1 ENGINE MEMORY
AS (SELECT 
  d.id,
  d.card_number,
  d.date,
  SUM(d.volume) AS volume,
  d.service,
  d.address_id,
  d1.id AS tmp_id,
  d1.date AS tmp_date,
  d1.volume AS tmp_volume
  FROM data d  
  LEFT JOIN 
  data d1 USING(card_number, address_id)
  WHERE d.volume > 0
  AND d1.id = (
    SELECT id FROM data 
    WHERE card_number = d.card_number 
      AND address_id = d.address_id 
      AND volume < 0 
      AND d.date > date 
    ORDER BY date DESC 
    LIMIT 1
  )
  GROUP BY d.id
  ORDER BY d.date ASC);
#=== Проверка что не осталось минусовых значений ===
#SELECT * FROM data d 
#  WHERE d.id NOT IN 
#(SELECT id FROM data_1 d1) AND d.volume > 0;

#=== Суммируем значения ===
UPDATE data d 
  INNER JOIN data_1 d1 ON d.id = d1.tmp_id 
  set d.volume = d.volume + d1.volume;
#=== Удаляем возвраты ===
DELETE FROM data WHERE id IN (SELECT id FROM data_1);
DROP TEMPORARY TABLE IF EXISTS data_1;
/*
В дампе имеются транзакции с 0 значением, по этой причине при выполнении запроса остается 1 запись
*/
SELECT 
  d.id,
  d.card_number,
  d.date,
  SUM(d.volume) AS volume,
  d.service,
  d.address_id,
  d1.id AS tmp_id,
  d1.date AS tmp_date,
  d1.volume AS tmp_volume
  FROM data d  
  LEFT JOIN 
  data d1 USING(card_number, address_id)
  WHERE d.volume > 0
  AND d1.id = (SELECT id FROM data WHERE card_number = d.card_number AND address_id = d.address_id AND volume < 0 AND d.date > date ORDER BY date DESC LIMIT 1)
  GROUP BY d.id
  ORDER BY d.date ASC;

```

**Комментарии по задаче**

1. Поскольку, в исходном дампе по ключам `card_number` и `address_id` не ко всем значениям с положительным балансам предшествовали значения со списанием, обработано было 73 записи из 184 (Оставшиеся записи имеют только положительные пополнения)
2. Порядковый номер поля `id` (автоинкремент) не соответствовал порядку в поле `date`. По этому построил выборку с сортировкой по полю `date`, а не по ключам

### Задание 2

Создать страницу со списком транзакций по топливным картам. Слева должна располагаться панель, где будет отображены информация о количестве транзакций по месяцам и по годам.
Сделать фильтр по номерам карт

**Пример**

- 2010 (27)
	- сентябрь (1)
	- июль (4)
	- июнь (7)
	- март (12)
	- февраль (3)
- 2009 (1)
	- июль (4)
	- июнь (7)

**РЕШЕНИЕ**

Рабочий пример: [Тыц сюда](http://fssp.selfieprinter.ru/id/)

**Коментарии к задаче**

1. Фильтр по году и месяцу реализован в левой панели (меню), по этому в таблице делать не стал


**Структура файлов**


	controllers/IdController.php	Контроллер со списком транзакций
	models/DataTable.php			Модель ActiveRecord
	models/DataTableSearch.php		Поисковая модель
	views/id/index.php				Вьюха
	ID20_task_1.sql					SQL запрос по первой задаче	










Тестовое задание 2
================

## Задача

Разработать систему перевода имен графических файлов с кириллицы на латиницу.

**Алгоритм скрипта**

1. Загружается ZIP файл на сервер с локального компьютера, и разархивируется в уникальную папку. 
2. Далее скрипт просматривает в папке и подпапках изображения, и если находит русские буквы в имени файла, то заменяет их на латиницу (перед заменой необходимо проверить, нет ли других файлов в этой папке с таким именем, и если есть, то выставить уникальный префикс). 
3. Далее необходимо просмотреть все html файлы из папки и подпапок на наличие тега img с этим русским именем картинки и заменить его на имя, подобранное шагом ранее. 
4. Далее искать следующие графические файлы с русскими буквами в имени и выполнять шаги 2-3. 
5. После того, как все имена картинок будут преобразованы, сжать обратно папку в zip формат и начать ее загрузку на локальный компьютер.

## Решение

[Рабочий пример](http://fssp.selfieprinter.ru/site/upload/)
Для распаковки файла используется команда
~~~
unzip file.zip -d folder
~~~

Т.к. стандартный класс ZipArchive не корректно работает с кодировкой имен фалйов Windows.
На некоторых серверах, возможно, необходимо будет обновить программу `unzip`

Структура файлов:
----------------

    components/ZipProcessor.php        Класс отвечающий за обработку загруженного ZIP файла
    controllers/SiteController.php     Контроллер с акшеном actionUpload - web-интерфейс загрузки файла
    views/site/upload.php              Отображение интерфейса загрузки файла
    models/UploadForm.php              Модель для валидации формы загрузки
    


Yii 2 Basic Project Template
============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-basic/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:^1.3.1"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~


### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.



TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
``` 

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 


### Running  acceptance tests

To execute acceptance tests do the following:  

1. Rename `tests/acceptance.suite.yml.example` to `tests/acceptance.suite.yml` to enable suite configuration

2. Replace `codeception/base` package in `composer.json` with `codeception/codeception` to install full featured
   version of Codeception

3. Update dependencies with Composer 

    ```
    composer update  
    ```

4. Download [Selenium Server](http://www.seleniumhq.org/download/) and launch it:

    ```
    java -jar ~/selenium-server-standalone-x.xx.x.jar
    ```

    In case of using Selenium Server 3.0 with Firefox browser since v48 or Google Chrome since v53 you must download [GeckoDriver](https://github.com/mozilla/geckodriver/releases) or [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) and launch Selenium with it:

    ```
    # for Firefox
    java -jar -Dwebdriver.gecko.driver=~/geckodriver ~/selenium-server-standalone-3.xx.x.jar
    
    # for Google Chrome
    java -jar -Dwebdriver.chrome.driver=~/chromedriver ~/selenium-server-standalone-3.xx.x.jar
    ``` 
    
    As an alternative way you can use already configured Docker container with older versions of Selenium and Firefox:
    
    ```
    docker run --net=host selenium/standalone-firefox:2.53.0
    ```

5. (Optional) Create `yii2_basic_tests` database and update it by applying migrations if you have them.

   ```
   tests/bin/yii migrate
   ```

   The database configuration can be found at `config/test_db.php`.


6. Start web server:

    ```
    tests/bin/yii serve
    ```

7. Now you can run all available tests

   ```
   # run all available tests
   vendor/bin/codecept run

   # run acceptance tests
   vendor/bin/codecept run acceptance

   # run only unit and functional tests
   vendor/bin/codecept run unit,functional
   ```

### Code coverage support

By default, code coverage is disabled in `codeception.yml` configuration file, you should uncomment needed rows to be able
to collect code coverage. You can run your tests and collect coverage with the following command:

```
#collect coverage for all tests
vendor/bin/codecept run -- --coverage-html --coverage-xml

#collect coverage only for unit tests
vendor/bin/codecept run unit -- --coverage-html --coverage-xml

#collect coverage for unit and functional tests
vendor/bin/codecept run functional,unit -- --coverage-html --coverage-xml
```

You can see code coverage output under the `tests/_output` directory.
